def subjectDecoder(file):
    '''
    Opens a specified file and loads it into a dictionary. Omit `.json`
    '''

    import json

    jsonfile = open(file + ".json", "r")
    jsonstr = jsonfile.read()
    jsonfile.close()

    return json.loads(jsonstr)

def packFinder(subject):
    '''
    Takes a subject and finds a pack to open, before turning it into a list of questions as well as the pack to open.
    '''

    for i in subject:
        print(subject)

    packToOpen = input("Which pack would you like to open? ")

    return [subject[packToOpen], packToOpen]