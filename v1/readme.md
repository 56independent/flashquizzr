# Introduction
This program is designed to accept JSON as input and ask questions based on it.

This program is designed for revision.

# Databases
Databases are in JSON format.

```json
{
    "a": [
        ["a", "a"],
        ["Question", "Answer"]
    ]
}
```

The questions are arranged into "packs", which are effectively a set of questions related to a single topic. Inside each pack, there are smaller lists which each contain:

| Index | Contents |
|-------|----------|
| 0     | Question |
| 1     | Answer   |
| 2     | List of times taken |

These databases can be written using the database maker provided.

| Command | Numeric Argument           |   Description                       |
|---------|----------------------------|-------------------------------------|
| A       |  none                      |   Add a question                    |
| H       |  none                      |   Prints help to terminal           |
| R       |  ID of question to remove  |   Remove a question                 |
| V       |  none                      |   View list of questions            |
| S       |  none                      |   Save the pack into the JSON file  |
| Q       |  none                      |   Quit                              |

The command reference is provided above.

# Questions
You may also be quizzed.

# Future Updates
This version is limited and must be expanded:

* Better writing to database libraries
* Recording of times
* Quizzing based on longest time
* Flask GUI