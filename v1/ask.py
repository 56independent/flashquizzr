def ask(listOfQuestions, amount):
    '''
    Asks a series of questions given:

    - A list of questions
    - Amount of questions to ask
    '''

    import random
    import time

    listOfQuestionsShaken = listOfQuestions

    random.shuffle(listOfQuestionsShaken)

    listOfQuestionsQuestions = listOfQuestionsShaken[:int(amount)]

    print(listOfQuestionsQuestions)

    # Ask the questions

    score = 0

    for i in range(len(listOfQuestionsQuestions)):
        o = i-1

        print(listOfQuestionsQuestions[o][0])
        print("")

        correctAnswer = listOfQuestionsQuestions[o][1]

        answerarea = random.randint(1,2)

        for i in range(3):
            if answerarea == i:
                print(correctAnswer)
            else:
                print(listOfQuestionsShaken[random.randint(0, len(listOfQuestionsShaken)-1)][1])

        print("")

        begin = time.time()
        answer = input("What is the answer? ")
        end = time.time()

        timeToAnswer = end-begin

        if len(correctAnswer) >= 3  and len(answer) >= 3:
            three = 2
        else:
            if len(correctAnswer) > len(answer):
                three = len(correctAnswer)
            else:
                three = len(answer)

        if answer[0: three] == correctAnswer[0: three]:
            print("Well done! (Answered in " + str(timeToAnswer) + " seconds)")
            score += 1

        else:
            print("The correct answer was  " + correctAnswer)

        input("Press enter to continue ")
        print("")
        print("------------")
        print("")

    print("you got a score of " + str(score) + " out of " + str(len(listOfQuestionsQuestions)) + " which is " + str((score/len(listOfQuestionsQuestions))*100) + "%")
