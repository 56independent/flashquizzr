def quizzer():
    '''
    Quizzes people
    '''
    import os

    import openers
    import ask

    content = os.listdir(".")

    print("")

    for i in range(len(content)):
        filename = content[i]

        if filename[len(filename) - 4:].lower() == "json":
            print("- " + filename)

    print("")

    ask.ask(openers.packFinder((openers.subjectDecoder(input("Which subject would you like to open? "))))[0], input("How many questions would you like to be asked? "))

import maker

choice = input("Would you like to be qu[i]zzed or make qu[e]stions? ")

if choice[0] == "i":
    quizzer()
elif choice[0] == "e":
    print("entered condition")
    maker.maker()