def maker():
    import os
    import json

    import openers

    print("Entered maker")

    def help():
        print("Command   Numeric Argument             Description")
        print("")
        print("A         none                         Add a question")
        print("H         none                         Prints help to terminal")
        print("R         ID of question to remove     Remove a question")
        print("V         none                         View list of questions")
        print("S         none                         Save the pack into the JSON file")
        print("Q         none                         Quit")

    def save():
        subjectContents[subjectReturned[1]] = pack

        jsonCode = json.JSONEncoder().encode(subjectContents)

        file = open(subject + ".json", "w")
        file.write(jsonCode)
        file.close

    def printQuestions(pack):
        print("")
        print("Questions:")
        print("")

        for i in range(len(pack)):
            print(str(i) + " - " + pack[i][0] + " - " + pack[i][1])

        print("")

    def commandParse(command):
        command = command.lower()

        print("")
        if command == "h":
            help()
        else:
            word = command[0].lower()

            try:
                argument = int(command[1].lower())
            except:
                argument = -1

            if word == "a":
                pack.append(["",""])
                pack[len(pack)-1][0] = (input("What is the question to ask? "))
                pack[len(pack)-1][1] = input("What is the Answer? ")
                printQuestions(pack)

            elif word == "r":
                pack.pop(pack[argument])
                printQuestions(pack)

            elif word == "s":
                save()

            elif word == "q":
                quit()

        print("")

    folder = input("Which folder contains your subjects? ")

    content = os.listdir(folder)

    print("")

    for i in range(len(content)):
        filename = content[i]

        if filename[len(filename) - 4:].lower() == "json":
            print("- " + filename)

    print("")

    subject = input("Which subject would you like to open (ommiting `.json`? ")

    try:
        subjectContents = openers.subjectDecoder(subject)
        subjectReturned = openers.packFinder(subjectContents)
        pack = subjectReturned[0]
    except:
        subjectContents = {}
        subjectReturned = ["",input("What is the pack name?")]

        pack = []

    printQuestions(pack)

    print("Entering Terminal. Press h for command reference.")
    print("")

    while True:
        commandParse(input("> "))