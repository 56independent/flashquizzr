def ask(nameOfSubject):
    import random
    import os
    import time
    import sys

    import unicodedata

    if "" not in sys.platform:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKCYAN = '\033[96m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
    else:
        HEADER = ''
        OKBLUE = ''
        OKCYAN = ''
        OKGREEN = ''
        WARNING = ''
        FAIL = ''
        ENDC = ''
        BOLD = ''
        UNDERLINE = ''


    import utils


    answersPrint = True

    def strip_accents(text):
        return ''.join(c for c in unicodedata.normalize('NFD', text)
                   if unicodedata.category(c) != 'Mn')

    def clearscreen():
        if "win" not in sys.platform:
            os.system("clear" + ENDC)
        else:
            os.system("cls" + ENDC) # Fuck you, Windows!

    def quiz(question):
        '''
        Accepts a question item and asks a question based off it before saving the time taken to answer
        '''

        # Ask the question
        print(question[0])
        print("" + ENDC)

        if type(question[1]) == str:
                correctAnswer = question[1]
        else:
            correctAnswer = []
            for i in range(len(question[1])):
                correctAnswer.append(question[1][i])

        answerarea = random.randint(1,2)

        listOfQuestionsShaken = packList
        random.shuffle(listOfQuestionsShaken)


        if answersPrint:
        # Print answers
            for i in range(3):
                if answerarea == i:
                    if type(correctAnswer) == str:
                        print(str(i) + " - " + correctAnswer)
                    else:
                        print(str(i) + " - " + correctAnswer[random.randint(0, len(correctAnswer)-1)])
                    answerNum = i
                else:
                    index = random.randint(0, len(listOfQuestionsShaken)-1)

                    if type(listOfQuestionsShaken[index][1]) == str:
                        print(str(i) + " - " + listOfQuestionsShaken[index][1])
                    else:
                        print(str(i) + " - " + listOfQuestionsShaken[index][1][random.randint(0, len(listOfQuestionsShaken[index][1])-1)])

            print("" + ENDC)

        answer = ""

        while not (0 <= 2 < len(answer) or len(answer) == len(correctAnswer) or str(answerNum) == answer):
            # Ask for an answer
            begin = time.time()
            answer = input("What is the answer? " + ENDC)
            end = time.time()

            timeToAnswer = round(end-begin, 3)

            # Find out if the answer is correct using first three chars (and give up if string is empty; this is CHEATING!)
            correct = False

            if (answer and 0 <= 2 < len(answer)) or len(answer) == len(correctAnswer) or str(answerNum) == answer:
                if type(correctAnswer) != str:
                    for i in range(len(correctAnswer)):
                        print(answer)
                        answer_is_correct = False

                        # Strip accents from the first `len(answer)` characters of `correctAnswer[i]` and convert to lowercase
                        cleaned_answer = strip_accents(correctAnswer[i][:len(answer)].lower())

                        if cleaned_answer == strip_accents(answer.lower()) or answer == str(answerNum):
                            correct = True
                else:
                    # Assuming `answer`, `correctAnswer`, and `ENDC` are already defined
                    answer_is_valid = False

                    # Strip accents from the first `len(answer)` characters of `correctAnswer` and convert both to lowercase
                    cleaned_answer = strip_accents(correctAnswer[:len(answer)].lower())
                    cleaned_typed_answer = strip_accents(answer.lower())

                    if cleaned_typed_answer == cleaned_answer and answer != "" + ENDC:
                        correct = True
            else:
                print(FAIL + "You entered \"" + answer + "\", which was invalid input. You need three characters at least" + ENDC)

        if correct:
            print(OKGREEN + "Well done! (Answered in " + str(timeToAnswer) + " seconds)" + ENDC)

            # Write time to answer
            whereIsTheQuestion = packList.index(question)

            try:
                packList[whereIsTheQuestion][2].append(timeToAnswer)
            except:
                packList[whereIsTheQuestion].append([timeToAnswer])

        else:
            if type(correctAnswer) == str:
                print(OKBLUE + "The correct answer was:" + ENDC)
                print(correctAnswer)
            else:
                print(OKBLUE + "The correct answers were:" + ENDC)
                print("" + ENDC)
                for o in range(len(correctAnswer)):
                    print(correctAnswer[o])
        # # Trim times
        # for i in packList:
        #     if int(packList.index(i)) <= len(packList)-3:
        #         packList.pop()

        # Save the answer
        subject[pack] = packList

        utils.subjectWriter(subject, nameOfSubject)

        # Continue after validation.
        if input("Press enter to continue " + ENDC) == "q":
            exit()
        else:
            clearscreen()

    subject = utils.subjectOpener(nameOfSubject)
    pack = utils.packFinder(subject)

    ran = False

    while True:

        # Delete data
        if ran:
            del(subject)
            del(packList)

        # Load up the pack
        subject = utils.subjectOpener(nameOfSubject)
        packList = utils.packOpener(nameOfSubject, pack)

        # Calculate average time to complete a question
        average = []
        for i in packList:
            # Example i: ["question", "answer", [4.5, 6.5, 2.1]], where the third item is the times.
            try:
                average.add(sum(packList[i][2]) / len(packList[i][2]))
            except:
                average.append(0)

        # Ask in order of biggest time or add new concept. Biggest time is a 3/4 chance.
        # All is ignored when * is used

            

        def print_question(i, packList):
            print("** New Information **")
            print()
            print("Question:")
            print(packList[i][0])
            print()
            if isinstance(packList[i][1], str):
                print("Answer:")
                print(packList[i][1])
            else:
                print("Answers:")
                print()
                for answer in packList[i][1]:
                    print(answer)
            print()

        def choose_question(packList, average):
            num = random.randint(2, 4)
            if num < 4:
                for i, avg in enumerate(average):
                    if avg == 0:
                        print_question(i, packList)
                        if input("Press enter to continue ") == "q":
                            quit()
                        quiz(packList[i])
                        return True
            else:
                index = average.index(max(average))
                quiz(packList[index])
                return True
            return False

        while True:
            choose_question(packList, average)
            clearscreen()

        print("Thanks for playing!")

        ran = True
