# USAGE:
#
# 1 - Put in a file in ARGV
# 2 - The file must contain a pipe seperating questions and answers. Seperate answers may be seperated using `","`.

import re
import sys

# Get filename from command line argument
if len(sys.argv) < 2:
    print("Usage: python script.py filename")
    sys.exit(1)

filename = sys.argv[1]

# Prompt for regular expression to search for and replacement string

pattern = re.compile('^"?(.*?)\\w*\\|\\w*(.*?)"?$')

# Read file contents
with open(filename, "r") as f:
    contents = f.read()

# Replace using regex
new_contents = pattern.sub('["\\1",["\\2"]]', contents)

# Save modified contents to new file
new_filename = filename + ".json"

with open(new_filename, "w") as f:
    f.write(new_contents)

print("File saved as", new_filename)
