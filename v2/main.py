import maker
import asker
import sys
import utils

for i in range(len(sys.argv)):
    try:
        if sys.argv[i] == "--mode":
            choice = sys.argv[i+1]
        if sys.argv[i] == "--file":
            file = sys.argv[i+1]
    except:
        print("You might have missed an argument after the flags!")

if "mode" not in locals():
    choice = input("Would you like to be [quizzed] or [make questions]? ")

if "file" not in locals():
    nameOfSubject = utils.subjectFinder()


if choice[0] == "q":
    asker.ask(nameOfSubject)

elif choice[0] == "m":
    maker.maker(nameOfSubject)
