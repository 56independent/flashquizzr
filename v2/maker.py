def maker(nameOfSubject):
    import os
    import json

    import utils

    def help():
        print("Command   Numeric Argument             Description")
        print("")
        print("A         none                         Add a question")
        print("H         none                         Prints help to terminal")
        print("R         ID of question to remove     Remove a question")
        print("V         none                         View list of questions")
        print("S         none                         Save the pack into the JSON file")
        print("Q         none                         Quit")
        print("C         1 - subject, 2 - pack        Switch subject or pack")

    def save():
        subject[pack] = packList

        utils.subjectWriter(subject, nameOfSubject)

    def commandParse(command):
        command = command.lower()

        print("")
        if command == "h":
            help()
        else:
            word = command[0].lower()

            try:
                argument = int(command[1 - 0:].lower())
            except:
                argument = -1

            if word == "a":
                if word == "a":
                    packList.append(["",""])
                packList[len(packList)-1][0] = (input("What is the question to ask? "))

                packList[len(packList)-1][1] = []

                for i in range(10000000):
                    answer = input("Answer (q at end to quit) " + str(i) + " > ")

                    packList[len(packList)-1][1].append(answer)

                    if answer == "" or answer == "q":
                        packList.pop(len(packList)-1)
                    if answer[-1] == "q" or answer == "q":
                        break

                utils.view(packList)

            elif word == "r":
                packList.pop(argument)
                utils.view(packList)

            elif word == "s":
                save()

            elif word == "q":
                quit()

            elif word == "v":
                utils.view(packList)

            elif word == "c":
                if number == 1:
                    csub()
                elif number == 2:
                    cpack()
                else:
                    print("invalid argument (choose 1 or 2)")

        print("")

    def csub(nameOfSubject):
        subject = utils.subjectOpener(nameOfSubject)
        pack = utils.packFinder(subject)

        return[nameOfSubject, subject, pack]

    def cpack(nameOfSubject, pack):
        # Load up the pack
        subject = utils.subjectOpener(nameOfSubject)
        packList = utils.packOpener(nameOfSubject, pack)

        return [subject, pack, packList]

    stuff1 = csub(nameOfSubject)
    stuff2 = cpack(stuff1[0], stuff1[2])

    nameOfSubject = stuff1[0]
    subject = stuff1[1]
    pack = stuff2[1]
    packList = stuff2[2]

    # See questions
    utils.view(packList)


    print("Entering Terminal. Press h for command reference.")
    print("")

    while True:
        commandParse(input("> "))
