def view(pack):
    import time
    import random

    print("")
    print("Questions:")
    print("")

    for i in range(len(pack)):
        if type(pack[i][1]) == str:
            print(str(i) + " - " + pack[i][0] + " - " + pack[i][1])
        else:
            for o in range(len(pack[i][1])):
                if o == 0:
                    beggining = str(i)
                else:
                    beggining = " "

                print(beggining + " - " + pack[i][0] + " - " + pack[i][1][o])


        time.sleep(random.randint(1, 150)/1000)

    print("")


def subjectWriter(subject, nameOfSubject):
    import json

    if "*" not in subject:
        jsonCode = json.dumps(subject, sort_keys=True, indent=4)

        file = open(nameOfSubject + ".json", "w", encoding="utf8")
        file.write(jsonCode)
        file.close()

def packOpener(subject, pack):
    import json

    allPacks = False

    if pack == "*":
        allPacks = True
    
    try:
        file = open(str(subject) + ".json", "r", encoding="utf8")
        jsonCode = file.read()

        subjectContents = json.loads(jsonCode)
        
    except:
        subjectContents = {}
        subjectContents[pack] = []

    if allPacks:
        questions = []
        for pack_name, pack_contents in subjectContents.items():
            if pack_name != "synopsis":
                questions += pack_contents

        return questions

    return subjectContents[pack]


def subjectOpener(subject):
    import json

    try:
        file = open(subject + ".json", "r", encoding="utf8")

        jsonCode = file.read()

        subjectContents = json.loads(jsonCode)
    except:
        if input("Met error! Is your JSON file wrong? (y/n)")[0] == "n":
            subjectContents = {}
        else:
            exit

    return subjectContents

def subjectFinder():
    import os
    import json

    print("")
    folder = input("Which folder contains your desired subjects? ")

    if folder == "":
        folder = "."

    content = os.listdir(folder)

    print("")

    for i in range(len(content)):
        filename = content[i]

        if filename[len(filename) - 4:].lower() == "json":
            print("- " + filename)
            with open(folder + "/" + filename, "r", encoding="utf8") as contents:
                contents = contents.read()
            try:
                synopsis = json.loads(contents)["synopsis"]
                print("  - " + synopsis)
            except:
                pass

    print("")

    subject = input("What subject would you like to open? ")
    print("")
    subject.replace(".json", "")

    return folder + "/" + subject

def packFinder(subject):
    print("")
    print("Packs available:")
    print("")

    for i in subject:
        if str(i) != "synopsis":
            times = []
            for o in range(len(subject[i])):
                try:
                    for l in range(len(subject[i][o][2])):
                        times.append(subject[i][o][2][l])
                except:
                    pass

            if len(times) != 0:
                average = sum(times)/len(times)
                average = round(average, 3)
            else:
                average = "none"

            print("- " + str(i) + " (Average time: " + str(average) + ")")

    print("")
    return input("Which pack would you like to open? ")
